# MICPAT：GPU Micro-architecture Independent Characteristic Profiling Analysis Tool  
&emsp;SIAT university  

## 1 Introduction  

&emsp;MICPAT is a micro-architecture independent characteristic profiling analysis tool based on NVBit (NVidia Binary Instrumentation Tool). MICPAT is a dynamic binary instrumentation library for NVIDIA GPUs which can inspect and modify the assembly code (SASS) of a GPU application 
without requiring recompilation, supporting Kepler, Maxwell, Pascal and Volta architectures (specifically SM >= 3.5 && SM <= 7.0). And MICPAT developed for computing with CUDA and OpenACC applications under x86-64 Linux. 

## 2 Getting Started with MICPAT

&emsp;MICPAT contains the README file and three folders:
1. A ```core``` folder contains the main static library ```libnvbit.a``` and various headers files, among all of these headers files, the ```nvbit.h``` file contains all the main APIs declarations.
2. A ```tools``` folder contains various source codes of MICPAT tools. Users can use the MICPAT tools to analyze the micro-architecture independent characteristics of applications.    
3. A ```test-application``` folder contains a rendering application that can be used to test MICPAT tools. This rendering application is a raytraced rendering of a schwarzchild black hole, which needs the SDL1.2, freeglut and CUDA libraries to compile.

&emsp;MICPAT tools must be compiled with nvcc version >= 8.0 , using GCC version >= 5.3.0. To compile the MICPAT tools simply type ```make``` inside the ```tools``` folder (make sure ```nvcc``` is in your PATH). Compile the test application by typing ```make``` inside the ```test-apps```  folder.

## 3 Using a MICPAT tool

&emsp;Before running an MICPAT tool, make sure ```nvdisasm``` is in your PATH. In Ubuntu distributions this is typically done by adding /usr/local/cuda/bin or /usr/local/cuda-"version"/bin to the PATH environment variable. To use an MICPAT tool we simply LD_PRELOAD the tool before the application execution command. For instance if the application runs natively as:   
```
./test-application/vectoradd  
``` 

&emsp;and produces the following output: 

![image](/uploads/3084c6b9f3b63592d2c50b95c111edfc/image.png)  

&emsp;we would use the MICPAT tool which performs basic block count as follow:

```
LD_PRELOAD=./tools/basic_block/basic_block.so ./test-application/vectoradd  
```

&emsp;The output for this command should be the following:

```no-highlight
------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
 core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
surface->w: 2048, surface->h = 2048
Pixel : 984325
inspecting ray_solver(float*, float*, float*, float*, float*, float*) - number basic blocks 51  
```

&emsp;As we can see, before the original output, there is a print showing the kernel 
call index "0", the kernel function prototype 
"ray_solver(float*, float*, float*, float*, float*, float*)", total number of basic blocks launched
 in this kernel "51".   

## 4 Introduction of MICPAT Tools

&emsp;As explained above, inside the ```tools``` folder there are five examples of 
MICPAT tools, we describe all of them in this README file:  

| Function    | Description               | Status                    |
| ------- | ------------------ | ----------------------- |
| instr_frequency  | Instructions shall be counted through thread level, in order to see the executed frequency of each instruction due to instruction set. Only through instruction frequency calculation can we optimize the execution efficiency of rendering programs.     | :ballot_box_with_check:   |
| memory_allocation | Calculating the memory size when the rendering programs is executed, and print the address of memory allocation and the data transfer between the device and host. | :ballot_box_with_check:   |
| basic_block  | A basic block is a sequence of statements executed in maximum order in a program, in which there is only one entry and exit. The entry is its first statement, and the exit is its last statement.     | :ballot_box_with_check:   |
| block_size  | Observing the warp schedule can help us know the distribution of grid size, block size, threads launched and shared memory, so that we can know the detailed launch units of GPU.     | :ballot_box_with_check:   |




## 5 Compilation line  

```no-highlight
	nvcc -std=c++11 -I../../src -Xptxas -cloning=no -maxrregcount=16 -Xcompiler 
			-Wall -arch=sm_35 -O3 -Xcompiler -fPIC -shared  
			basic_block.cu -L../../src -lcuda -o basic_block.so
```
&emsp;The use "-arch=sm_35" is not required, but is typically done so the same 
pre-compiled MICPAT tool can be used across multiple GPU generations >= SM 3.5. 
If that is not a requirement then the MICPAT tool can be compiled for a specific 
architecture.

